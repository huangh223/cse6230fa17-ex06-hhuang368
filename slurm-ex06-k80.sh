#!/bin/sh
#SBATCH  -J ex06-k80                     # Job name
#SBATCH  -p GPU-shared                   # Queue (RM, RM-shared, GPU, GPU-shared)
#SBATCH  -N 1                            # Number of nodes
#SBATCH --gres=gpu:k80:1                 # GPU type and amount
#SBATCH  -t 00:10:00                     # Time limit hrs:min:sec
#SBATCH  -o ex06-k80-%j.out              # Standard output and error log

module use /home/tisaac/opt/modulesfiles
module load petsc/cse6230-double

make ex06

git rev-parse HEAD

git diff-files

./ex06 -num_steps
